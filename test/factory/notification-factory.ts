import { Content } from '@app/entity/content';
import { INotification, Notification } from '@app/entity/notification';

type Overwrite = Partial<INotification>;

const makeNotification = (overwrite: Overwrite = {}) => {
  return new Notification({
    recipient_id: 'fake-recipient',
    content: new Content('fake-content'),
    category: 'fake-category',
    created_at: new Date(),
    ...overwrite,
  });
};

export { makeNotification };
