import { randomUUID } from 'node:crypto';
import { Notification } from '../src/app/entity/notification';
import { NotificationRepository } from '@app/repository/notification-repository';
import { NotificationNotFoundError } from '@app/use-case/error/notification-not-found-error';

class InMemoryRepository implements NotificationRepository {
  public notifications: Notification[] = [];

  async findById(id: string): Promise<Notification | null> {
    return this.notifications.find((n) => n.id === id) || null;
  }

  async update(notification: Notification): Promise<Notification> {
    const idx = this.notifications.findIndex((n) => n.id === notification.id);

    if (idx < 0) {
      throw new NotificationNotFoundError();
    }

    this.notifications[idx] = notification;
    return notification;
  }

  async create(notification: Notification): Promise<Notification> {
    const not = new Notification({
      id: randomUUID(),
      category: notification.category,
      content: notification.content,
      recipient_id: notification.recipient_id,
      read_at: notification.read_at,
      created_at: new Date(),
    });
    this.notifications.push(not);
    return not;
  }

  async list(): Promise<Notification[]> {
    return this.notifications;
  }

  async countByRecipientId(recipient_id: string): Promise<number> {
    return this.notifications.filter((n) => n.recipient_id === recipient_id)
      .length;
  }

  async findManyByRecipientId(recipient_id: string): Promise<Notification[]> {
    return this.notifications.filter((n) => n.recipient_id === recipient_id);
  }
}

export { InMemoryRepository };
