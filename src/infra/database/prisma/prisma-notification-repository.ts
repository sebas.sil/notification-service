import { NotificationNotFoundError } from '@app/use-case/error/notification-not-found-error';
import { Injectable } from '@nestjs/common';
import { Notification } from 'src/app/entity/notification';
import { NotificationRepository } from 'src/app/repository/notification-repository';
import { PrismaNotificationMapper } from './prisma-notification-mapper';
import { PrismaService } from './prisma.service';

@Injectable()
class PrismaNotificationRepository implements NotificationRepository {
  constructor(readonly prisma: PrismaService) {}

  async list(): Promise<Notification[]> {
    const rawNotifications = await this.prisma.notification.findMany();
    return rawNotifications.map(PrismaNotificationMapper.toDomain);
  }

  async findById(id: string): Promise<Notification | null> {
    const rawNotification = await this.prisma.notification.findUnique({
      where: {
        id,
      },
    });

    if (!rawNotification) {
      throw new NotificationNotFoundError();
    }

    return PrismaNotificationMapper.toDomain(rawNotification);
  }

  async countByRecipientId(recipient_id: string): Promise<number> {
    return this.prisma.notification.count({
      where: { recipient_id },
    });
  }

  async findManyByRecipientId(recipient_id: string): Promise<Notification[]> {
    const rawNotifications = await this.prisma.notification.findMany({
      where: {
        recipient_id,
      },
    });
    return rawNotifications.map(PrismaNotificationMapper.toDomain);
  }

  async create(notification: Notification): Promise<Notification> {
    const data = PrismaNotificationMapper.toPrisma(notification);
    const created = await this.prisma.notification.create({ data });

    return PrismaNotificationMapper.toDomain(created);
  }

  async update(notification: Notification): Promise<Notification> {
    const data = PrismaNotificationMapper.toPrisma(notification);
    const created = await this.prisma.notification.update({
      where: {
        id: notification.id,
      },
      data,
    });

    return PrismaNotificationMapper.toDomain(created);
  }
}

export { PrismaNotificationRepository };
