import { Notification as PrismaEntity } from '@prisma/client';
import { Notification } from '@app/entity/notification';
import { randomUUID } from 'node:crypto';
import { Content } from '@app/entity/content';

class PrismaNotificationMapper {
  static toPrisma(notification: Notification) {
    return {
      id: notification.id ?? randomUUID(),
      category: notification.category,
      content: notification.content.value,
      recipient_id: notification.recipient_id,
      read_at: notification.read_at,
      canceled_at: notification.canceled_at,
    };
  }

  static toDomain(notification: PrismaEntity): Notification {
    return new Notification({
      id: notification.id,
      recipient_id: notification.recipient_id,
      content: new Content(notification.content),
      category: notification.category,
      created_at: notification.created_at,
      canceled_at: notification.canceled_at,
      read_at: notification.read_at,
    });
  }
}

export { PrismaNotificationMapper };
