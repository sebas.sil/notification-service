import { Module } from '@nestjs/common';
import { NotificationRepository } from 'src/app/repository/notification-repository';
import { PrismaNotificationRepository } from './prisma/prisma-notification-repository';
import { PrismaService } from './prisma/prisma.service';

@Module({
  providers: [
    PrismaService,
    {
      provide: NotificationRepository,
      useClass: PrismaNotificationRepository,
    },
  ],
  exports: [NotificationRepository],
})
class DatabaseModule {}

export { DatabaseModule };
