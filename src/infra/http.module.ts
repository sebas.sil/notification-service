import { CancelNotification } from '@app/use-case/uc-cancel-notification';
import { CountRecipientNotification } from '@app/use-case/uc-count-recipient-notification';
import { GetRecipientNotification } from '@app/use-case/uc-get-recipient-notification';
import { ReadNotification } from '@app/use-case/uc-read-notification';
import { UnreadNotification } from '@app/use-case/uc-unread-notification';
import { Module } from '@nestjs/common';
import { SendNotification } from 'src/app/use-case/uc-send-notification';
import { DatabaseModule } from './database/database.module';
import { NotificationController } from './http/controller/notification.controller';

@Module({
  imports: [DatabaseModule],
  controllers: [NotificationController],
  providers: [
    SendNotification,
    CancelNotification,
    CountRecipientNotification,
    GetRecipientNotification,
    ReadNotification,
    UnreadNotification,
  ],
})
class HttpModule {}

export { HttpModule };
