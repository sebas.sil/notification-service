import { Notification } from '@app/entity/notification';

class HTTPResponseMapper {
  static toHTTP(notification: Notification) {
    return {
      id: notification.id,
      recipient_id: notification.recipient_id,
      content: notification.content.value,
      category: notification.category,
      read_at: notification.read_at,
      created_at: notification.created_at,
      canceled_at: notification.canceled_at,
    };
  }
}

export { HTTPResponseMapper };
