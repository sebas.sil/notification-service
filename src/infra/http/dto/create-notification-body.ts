import { IsNotEmpty, IsUUID, Length } from 'class-validator';

export class CreateNotificationBody {
  @IsNotEmpty()
  @IsUUID()
  recipient_id: string;

  @IsNotEmpty()
  @Length(4, 160)
  content: string;

  @IsNotEmpty()
  @Length(5, 10)
  category: string;
}
