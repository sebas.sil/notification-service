import { CancelNotification } from '@app/use-case/uc-cancel-notification';
import { CountRecipientNotification } from '@app/use-case/uc-count-recipient-notification';
import { GetRecipientNotification } from '@app/use-case/uc-get-recipient-notification';
import { ReadNotification } from '@app/use-case/uc-read-notification';
import { UnreadNotification } from '@app/use-case/uc-unread-notification';
import {
  Body,
  Controller,
  Get,
  Header,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { SendNotification } from 'src/app/use-case/uc-send-notification';
import { CreateNotificationBody } from '../dto/create-notification-body';
import { HTTPResponseMapper } from '../http-notification-mapper';

@Controller('notification')
class NotificationController {
  constructor(
    private readonly sendUC: SendNotification,
    private readonly cancelUC: CancelNotification,
    private readonly countUC: CountRecipientNotification,
    private readonly listUC: GetRecipientNotification,
    private readonly readUC: ReadNotification,
    private readonly unreadUC: UnreadNotification,
  ) {}

  @Post()
  @Header('Content-Type', 'application/json')
  async create(@Body() body: CreateNotificationBody) {
    const { content, recipient_id, category } = body;
    const { notification } = await this.sendUC.execute({
      category,
      content,
      recipient_id,
    });

    return { notification: HTTPResponseMapper.toHTTP(notification) };
  }

  @Patch(':id/cancel')
  @Header('Content-Type', 'application/json')
  async cancel(@Param('id') id: string) {
    const { notification } = await this.cancelUC.execute({ id });
    return { notification: HTTPResponseMapper.toHTTP(notification) };
  }

  @Get(':recipient_id/count')
  @Header('Content-Type', 'application/json')
  async count(@Param('recipient_id') recipient_id: string) {
    const { count } = await this.countUC.execute({ recipient_id });
    return {
      count,
    };
  }

  @Get(':recipient_id/list')
  @Header('Content-Type', 'application/json')
  async get(@Param('recipient_id') recipient_id: string) {
    const { notifications } = await this.listUC.execute({ recipient_id });
    return { notifications: notifications.map(HTTPResponseMapper.toHTTP) };
  }

  // FIXME read only the own notifications
  @Patch(':id/read')
  @Header('Content-Type', 'application/json')
  async read(@Param('id') id: string) {
    const { notification } = await this.readUC.execute({ id });
    return { notification: HTTPResponseMapper.toHTTP(notification) };
  }

  // FIXME unread only the own notifications
  @Patch(':id/unread')
  @Header('Content-Type', 'application/json')
  async unread(@Param('id') id: string) {
    const { notification } = await this.unreadUC.execute({ id });
    return { notification: HTTPResponseMapper.toHTTP(notification) };
  }
}

export { NotificationController };
