import { Notification } from '../entity/notification';

abstract class NotificationRepository {
  abstract create(notification: Notification): Promise<Notification>;
  abstract list(): Promise<Notification[]>;
  abstract findById(id: string): Promise<Notification | null>;
  abstract update(notification: Notification): Promise<Notification>;
  abstract countByRecipientId(recipient_id: string): Promise<number>;
  abstract findManyByRecipientId(recipient_id: string): Promise<Notification[]>;
}

export { NotificationRepository };
