import { Injectable } from '@nestjs/common';
import { Notification } from '../entity/notification';
import { NotificationRepository } from '../repository/notification-repository';
import { NotificationNotFoundError } from './error/notification-not-found-error';
import {
  UCNotificationRequest,
  UCNotification,
  UCNotificationResponse,
} from './uc-notification';

interface CancelNotificationRequest extends UCNotificationRequest {
  id: string;
}

interface CancelNotificationResponse extends UCNotificationResponse {
  notification: Notification;
}

@Injectable()
class CancelNotification implements UCNotification {
  constructor(readonly repository: NotificationRepository) {}

  async execute(
    request: CancelNotificationRequest,
  ): Promise<CancelNotificationResponse> {
    const { id } = request;

    let not = await this.repository.findById(id);
    if (!not) {
      throw new NotificationNotFoundError();
    }

    not.cancel();
    not = await this.repository.update(not);
    return { notification: not };
  }
}

export { CancelNotification };
