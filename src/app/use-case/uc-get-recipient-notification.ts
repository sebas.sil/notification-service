import { Injectable } from '@nestjs/common';
import { Notification } from '../entity/notification';
import { NotificationRepository } from '../repository/notification-repository';
import {
  UCNotificationRequest,
  UCNotification,
  UCNotificationResponse,
} from './uc-notification';

interface GetRecipientNotificationRequest extends UCNotificationRequest {
  recipient_id: string;
}

interface GetRecipientNotificationResponse extends UCNotificationResponse {
  notifications: Notification[];
}

@Injectable()
class GetRecipientNotification implements UCNotification {
  constructor(readonly repository: NotificationRepository) {}

  async execute(
    request: GetRecipientNotificationRequest,
  ): Promise<GetRecipientNotificationResponse> {
    const { recipient_id } = request;

    const notifications = await this.repository.findManyByRecipientId(
      recipient_id,
    );

    return { notifications };
  }
}

export { GetRecipientNotification };
