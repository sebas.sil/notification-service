class NotificationNotFoundError extends Error {
  constructor() {
    super('Notification not found');
  }
}

export { NotificationNotFoundError };
