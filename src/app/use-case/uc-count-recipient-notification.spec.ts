import { makeNotification } from '@test/factory/notification-factory';
import { InMemoryRepository } from '../../../test/in-memory-repository';
import { NotificationRepository } from '../repository/notification-repository';
import { CountRecipientNotification } from './uc-count-recipient-notification';

let repo: NotificationRepository;

beforeEach(() => {
  repo = new InMemoryRepository();
});

describe('Count notification', () => {
  it('should be able to Count notification', async () => {
    const countNotification = new CountRecipientNotification(repo);
    let notification = makeNotification({ recipient_id: 'fake-recipient' });

    await repo.create(notification);
    await repo.create(notification);

    notification = makeNotification({ recipient_id: 'another-recipient' });

    await repo.create(notification);

    const p1 = countNotification.execute({
      recipient_id: 'fake-recipient',
    });
    const p2 = countNotification.execute({
      recipient_id: 'another-recipient',
    });
    const p3 = countNotification.execute({
      recipient_id: 'none-recipient',
    });

    expect(p1).resolves.toMatchObject({ count: 2 });
    expect(p2).resolves.toMatchObject({ count: 1 });
    expect(p3).resolves.toMatchObject({ count: 0 });
  });
});
