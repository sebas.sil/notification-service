interface UCNotificationRequest {}
interface UCNotificationResponse {}

interface UCNotification {
  execute(request: UCNotificationRequest): Promise<UCNotificationResponse>;
}

export type { UCNotificationRequest, UCNotificationResponse, UCNotification };
