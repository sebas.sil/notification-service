import { Injectable } from '@nestjs/common';
import { NotificationRepository } from '../repository/notification-repository';
import {
  UCNotificationRequest,
  UCNotification,
  UCNotificationResponse,
} from './uc-notification';

interface CountRecipientNotificationRequest extends UCNotificationRequest {
  recipient_id: string;
}

interface CountRecipientNotificationResponse extends UCNotificationResponse {
  count: number;
}

@Injectable()
class CountRecipientNotification implements UCNotification {
  constructor(readonly repository: NotificationRepository) {}

  async execute(
    request: CountRecipientNotificationRequest,
  ): Promise<CountRecipientNotificationResponse> {
    const { recipient_id } = request;

    const count = await this.repository.countByRecipientId(recipient_id);

    return { count };
  }
}

export { CountRecipientNotification };
