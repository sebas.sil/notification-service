import { makeNotification } from '@test/factory/notification-factory';
import { InMemoryRepository } from '../../../test/in-memory-repository';
import { NotificationRepository } from '../repository/notification-repository';
import { GetRecipientNotification } from './uc-get-recipient-notification';

let repo: NotificationRepository;

beforeEach(() => {
  repo = new InMemoryRepository();
});

describe('Cancel notification', () => {
  it('should be able to get notifications', async () => {
    const getNotifications = new GetRecipientNotification(repo);
    const notification = makeNotification({ recipient_id: 'fake-id' });
    await repo.create(notification);
    await repo.create(notification);
    const another_notification = makeNotification({ recipient_id: 'fake-id2' });
    await repo.create(another_notification);

    const { notifications } = await getNotifications.execute({
      recipient_id: 'fake-id',
    });

    expect(notifications).toBeTruthy();
    expect(notifications).toHaveLength(2);
    expect(notifications).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ recipient_id: 'fake-id' }),
        expect.objectContaining({ recipient_id: 'fake-id' }),
      ]),
    );
  });

  it('should return an empty array', async () => {
    const getNotifications = new GetRecipientNotification(repo);
    const notification = makeNotification({ recipient_id: 'fake-id' });
    await repo.create(notification);
    await repo.create(notification);
    const another_notification = makeNotification({ recipient_id: 'fake-id2' });
    await repo.create(another_notification);

    const { notifications } = await getNotifications.execute({
      recipient_id: 'none-id',
    });

    expect(notifications).toBeTruthy();
    expect(notifications).toHaveLength(0);
  });
});
