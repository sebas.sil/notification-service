import { Injectable } from '@nestjs/common';
import { Content } from '../entity/content';
import { Notification } from '../entity/notification';
import { NotificationRepository } from '../repository/notification-repository';
import {
  UCNotificationRequest,
  UCNotification,
  UCNotificationResponse,
} from './uc-notification';

interface SendNotificationRequest extends UCNotificationRequest {
  recipient_id: string;
  content: string;
  category: string;
}

interface SendNotificationResponse extends UCNotificationResponse {
  notification: Notification;
}

@Injectable()
class SendNotification implements UCNotification {
  constructor(readonly repository: NotificationRepository) {}

  async execute(
    request: SendNotificationRequest,
  ): Promise<SendNotificationResponse> {
    const { recipient_id, content, category } = request;
    const notification = new Notification({
      recipient_id,
      content: new Content(content),
      category,
      created_at: new Date(),
    });

    const not = await this.repository.create(notification);

    return { notification: not };
  }
}

export { SendNotification };
