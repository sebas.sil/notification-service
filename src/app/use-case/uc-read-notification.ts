import { Injectable } from '@nestjs/common';
import { Notification } from '../entity/notification';
import { NotificationRepository } from '../repository/notification-repository';
import { NotificationNotFoundError } from './error/notification-not-found-error';
import {
  UCNotificationRequest,
  UCNotification,
  UCNotificationResponse,
} from './uc-notification';

interface ReadNotificationRequest extends UCNotificationRequest {
  id: string;
}

interface ReadNotificationResponse extends UCNotificationResponse {
  notification: Notification;
}

@Injectable()
class ReadNotification implements UCNotification {
  constructor(readonly repository: NotificationRepository) {}

  async execute(
    request: ReadNotificationRequest,
  ): Promise<ReadNotificationResponse> {
    const { id } = request;

    let notification = await this.repository.findById(id);

    if (!notification) {
      throw new NotificationNotFoundError();
    }
    notification.read();
    notification = await this.repository.update(notification);

    return { notification };
  }
}

export { ReadNotification };
