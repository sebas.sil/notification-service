import { makeNotification } from '@test/factory/notification-factory';
import { InMemoryRepository } from '../../../test/in-memory-repository';
import { NotificationRepository } from '../repository/notification-repository';
import { UnreadNotification } from './uc-unread-notification';

jest
  .useFakeTimers({ doNotFake: ['performance'] })
  .setSystemTime(new Date('2022-08-31 03:22:10').getTime());

let repo: NotificationRepository;

beforeEach(() => {
  repo = new InMemoryRepository();
});

describe('Read notification', () => {
  it('should be able to read notification', async () => {
    const unreadNotification = new UnreadNotification(repo);
    const notification = makeNotification({ read_at: new Date() });

    const created = await repo.create(notification);

    if (!created.id) {
      throw Error('Should have ID');
    }
    const { notification: notif } = await unreadNotification.execute({
      id: created.id,
    });

    const notList = await repo.list();

    expect(notif).toBeTruthy();
    expect(notList[0].read_at).toBeNull();
  });

  it('should not be able to read notification when not found', async () => {
    const unreadNotification = new UnreadNotification(repo);
    const notification = makeNotification();

    await repo.create(notification);

    expect(unreadNotification.execute({ id: 'fake-id' })).rejects.toThrowError(
      'Notification not found',
    );
  });
});
