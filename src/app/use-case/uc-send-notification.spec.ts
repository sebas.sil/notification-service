import { InMemoryRepository } from '../../../test/in-memory-repository';
import { NotificationRepository } from '../repository/notification-repository';
import { SendNotification } from './uc-send-notification';

jest
  .useFakeTimers({ doNotFake: ['performance'] })
  .setSystemTime(new Date('2022-08-31 03:22:10').getTime());

let repo: NotificationRepository;

beforeEach(() => {
  repo = new InMemoryRepository();
});

describe('send notification', () => {
  it('should be able to send notification', async () => {
    const sendNotification = new SendNotification(repo);
    const { notification: notif } = await sendNotification.execute({
      content: 'UC content test',
      category: 'UC category test',
      recipient_id: 'UC recipient test',
    });

    expect(notif).toBeTruthy();
    expect(repo.list()).resolves.toHaveLength(1);
    expect('UC recipient test').toBe(notif.recipient_id);
    expect('UC category test').toBe(notif.category);
    expect({ content: 'UC content test' }).toMatchObject(notif.content);
    expect('2022-08-31T06:22:10.000Z').toBe(notif.created_at.toISOString());
    expect(repo.list()).resolves.toEqual([notif]);
  });
});
