import { makeNotification } from '@test/factory/notification-factory';
import { InMemoryRepository } from '../../../test/in-memory-repository';
import { NotificationRepository } from '../repository/notification-repository';
import { CancelNotification } from './uc-cancel-notification';

jest
  .useFakeTimers({ doNotFake: ['performance'] })
  .setSystemTime(new Date('2022-08-31 03:22:10').getTime());

let repo: NotificationRepository;

beforeEach(() => {
  repo = new InMemoryRepository();
});

describe('Cancel notification', () => {
  it('should be able to Cancel notification', async () => {
    const cancelNotification = new CancelNotification(repo);
    const notification = makeNotification();

    const created = await repo.create(notification);
    if (!created.id) {
      throw Error('Should have ID');
    }
    const { notification: notif } = await cancelNotification.execute({
      id: created.id,
    });

    const notList = await repo.list();

    expect(notif).toBeTruthy();
    expect(notList[0].canceled_at?.toISOString()).toBe(
      '2022-08-31T06:22:10.000Z',
    );
  });

  it('should not be able to Cancel notification when not found', async () => {
    const cancelNotification = new CancelNotification(repo);
    const notification = makeNotification();

    await repo.create(notification);

    expect(cancelNotification.execute({ id: 'fake-id' })).rejects.toThrowError(
      'Notification not found',
    );
  });
});
