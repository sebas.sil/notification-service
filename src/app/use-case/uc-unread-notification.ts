import { Injectable } from '@nestjs/common';
import { Notification } from '../entity/notification';
import { NotificationRepository } from '../repository/notification-repository';
import { NotificationNotFoundError } from './error/notification-not-found-error';
import {
  UCNotificationRequest,
  UCNotification,
  UCNotificationResponse,
} from './uc-notification';

interface UnreadNotificationRequest extends UCNotificationRequest {
  id: string;
}

interface UnreadNotificationResponse extends UCNotificationResponse {
  notification: Notification;
}

@Injectable()
class UnreadNotification implements UCNotification {
  constructor(readonly repository: NotificationRepository) {}

  async execute(
    request: UnreadNotificationRequest,
  ): Promise<UnreadNotificationResponse> {
    const { id } = request;

    let notification = await this.repository.findById(id);

    if (!notification) {
      throw new NotificationNotFoundError();
    }
    notification.unread();
    notification = await this.repository.update(notification);

    return { notification };
  }
}

export { UnreadNotification };
