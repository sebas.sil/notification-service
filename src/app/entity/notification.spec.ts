import { Content } from './content';
import { Notification } from './notification';

jest
  .useFakeTimers({ doNotFake: ['performance'] })
  .setSystemTime(new Date('2022-08-31 03:22:10').getTime());

describe('notification content', () => {
  it('should be able to create a notification', () => {
    const notif = new Notification({
      recipient_id: 'simple id',
      content: new Content('simple content'),
      category: 'simple category',
      created_at: new Date(),
    });

    expect(notif).toBeTruthy();
    expect(notif.recipient_id).toBe('simple id');
    expect(notif.category).toBe('simple category');
    expect(notif.content).toMatchObject({ content: 'simple content' });
    expect(notif.created_at.toISOString()).toBe('2022-08-31T06:22:10.000Z');
  });
});
