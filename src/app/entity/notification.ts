import { BaseEntity } from './base-entity';
import { Content } from './content';

interface INotification {
  id?: string;
  recipient_id: string;
  content: Content;
  category: string;
  read_at?: Date | null;
  created_at: Date;
  canceled_at?: Date | null;
}

class Notification extends BaseEntity {
  private props: INotification;

  constructor(props: INotification) {
    super(props.id);
    this.props = props;
  }

  public set recipient_id(recipient_id: string) {
    this.props.recipient_id = recipient_id;
  }
  public get recipient_id(): string {
    return this.props.recipient_id;
  }

  public set content(content: Content) {
    this.props.content = content;
  }
  public get content(): Content {
    return this.props.content;
  }

  public set category(category: string) {
    this.props.category = category;
  }
  public get category(): string {
    return this.props.category;
  }

  public get read_at(): Date | null | undefined {
    return this.props.read_at;
  }

  public get created_at(): Date {
    return this.props.created_at;
  }

  public get canceled_at(): Date | null | undefined {
    return this.props.canceled_at;
  }

  public cancel() {
    this.props.canceled_at = new Date();
  }
  public read() {
    this.props.read_at = new Date();
  }
  public unread() {
    this.props.read_at = null;
  }
}

export type { INotification };
export { Notification };
