class BaseEntity {
  constructor(private readonly _id?: string) {}

  public get id() {
    return this._id;
  }
}

export { BaseEntity };
