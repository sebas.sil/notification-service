import { Content } from './content';

describe('notification content', () => {
  it('should be able to create a notification content', () => {
    const content = new Content('Este é um conteúdo válido');

    expect(content).toBeTruthy();
  });

  it('should not be able to create a notification content with less than 5 characters', () => {
    expect(() => new Content('')).toThrowError(
      'Content length should be between 5 and 140 characters',
    );
  });

  it('should not be able to create a notification content with more than 140 characters', () => {
    expect(() => new Content('a'.repeat(141))).toThrowError(
      'Content length should be between 5 and 140 characters',
    );
  });
});
